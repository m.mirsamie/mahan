<?php
    date_default_timezone_set("Asia/Tehran");
    class SoapClientDebug extends SoapClient
    {
      public function __doRequest($request, $location, $action, $version, $one_way = 0) {
          // Add code to inspect/dissect/debug/adjust the XML given in $request here
          //var_dump($request);
          // Uncomment the following line, if you actually want to do the request
          return parent::__doRequest($request, $location, $action, $version, $one_way);
          //return('');
      }
    }

    function generateWSSecurity($user, $password)
    {
        $tm_created = gmdate('Y-m-d\TH:i:s\Z');
        $tm_expires = gmdate('Y-m-d\TH:i:s\Z', gmdate('U') + 180);
        $simple_nonce = mt_rand();
        $encoded_nonce = base64_encode(pack('H*', $simple_nonce));
        $passdigest = base64_encode(pack('H*',sha1(pack('H*', $simple_nonce) . pack('a*', $tm_created) . pack('a*', $password))));
        $ns_envelope = 'http://schemas.xmlsoap.org/soap/envelope/';
        $ns_wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        $ns_wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
        $password_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';
        $encoding_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary';
        $root = new \SimpleXMLElement('<root/>');
        $envelope = $root->addChild('soap:Envelope', null, $ns_envelope);
        $soapheader = $envelope->addChild('soap:Header');
        $security = $soapheader->addChild('wsse:Security', null,$ns_wsse);
        $security->addAttribute('soap:mustUnderstand','1');
        $usernameToken = $security->addChild('wsse:UsernameToken', null, $ns_wsse);
        $usernameToken->addChild('wsse:Username', $user, $ns_wsse);
        $password = $usernameToken->addChild('wsse:Password', $password, $ns_wsse);
        $password->addAttribute('Type', $password_type);
        $root->registerXPathNamespace('soap', $ns_envelope);
        $full = $root->xpath('/root/soap:Envelope');
        $auth = $full[0]->asXML();
        return $auth;
    }
    function soapClientWSSecurityHeader($user, $password)
    {
        return new \SoapHeader('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd',
                'Security', new \SoapVar(generateWSSecurity($user, $password), XSD_ANYXML), true
        );
    }

    
    $TerminalID = "MHD213";
    $AgencyUser = "WSYASTOOSKBL";
    $AgencyPass = 'p@ss1234';
    $adl = 1;
    $chd = 0;
    $inf = 0;
    //$BookData = createBookData($adl,$chd,$inf);
    $DepDate = "2015-11-16T18:20:00";
    $AriDate = "2015-11-16T20:05:00";
    $from = "IKA";
    $to = "IST";
    $fnumber = "W5114";
    $RPH = 'W5$IKA/IST$201664$20150802182000$20150802200500';
    
    
    $client = new SoapClient("http://mahanair.isaaviations.com/webservices/services/AAResWebServices?wsdl",array("trace"=>1));
    $client->__setSoapHeaders(soapClientWSSecurityHeader($AgencyUser, $AgencyPass));
    
    //var_dump($client->__getFunctions());
    $AirIt = "<ns1:AirItinerary><ns1:OriginDestinationOptions><ns1:OriginDestinationOption>"
            . '<ns1:FlightSegment ArrivalDateTime="'.$AriDate.'" DepartureDateTime="'.$DepDate.'" FlightNumber="'.$fnumber.'"  RPH="'.$RPH.'" >' //ResBookDesigCode="Q" NumberInParty="1" Status="30">'
            . '<ns1:DepartureAirport LocationCode="'.$from.'" />'
            . '<ns1:ArrivalAirport LocationCode="'.$to.'"  />'
            //. '<ns1:MarketingAirline Code="W5"/>'
            . '</ns1:FlightSegment>'
            . "</ns1:OriginDestinationOption></ns1:OriginDestinationOptions></ns1:AirItinerary>";
    /*
    $PriceInfo = '<ns1:PriceInfo>'
            . '<ns1:ItinTotalFare>'
            . '<ns1:TotalFare Amount="381.30" CurrencyCode="USD" DecimalPlaces="2" />'
            . '<ns1:TPA_Extensions>'
            . '<ns1:ExpectedPriceRange min="375.00" max="390.00" />'
            . '<ns1:Customer>'
            . '<ns1:PersonName>'
            . '<ns1:GivenName>Juniper</ns1:GivenName>'
            . '<ns1:Surname>Test</ns1:Surname>'
            . '</ns1:PersonName>'
            . '<ns1:Telephone PhoneNumber="(703)5555555"/>'
            . '<ns1:Document DocID="12345678A" />'
            . '<ns1:Email>user@provider.com</ns1:Email>'
            . '<ns1:Address>'
            . '<ns1:AddressLine>My address</ns1:AddressLine>'
            . '<ns1:CityName>Palma de Mallorca</ns1:CityName>'
            . '<ns1:PostalCode>07000</ns1:PostalCode>'
            . '<ns1:CountryName Code="ES">Spain</ns1:CountryName>'
            . '<ns1:/Address>'
            . '</ns1:Customer>'
            . '</ns1:TPA_Extensions>'
            . '</ns1:ItinTotalFare>'
            . '<ns1:FareInfos>'
            . '<ns1:FareInfo CurrencyCode="USD">'
            . '<ns1:FareInfo TripType="Roundtrip">'
            . '<ns1:Fare TotalFare="381.30" />'
            . '</ns1:FareInfo>'
            . '</ns1:FareInfo>'
            . '</ns1:FareInfos>'
            . '</ns1:PriceInfo>';
     * 
     */
    $TPA = '<ns1:TPA_Extensions/>';
            /*
            . '<ns1:ExpectedPriceRange min="375.00" max="390.00" />'
            . '<ns1:Customer>'
            . '<ns1:PersonName>'
            . '<ns1:GivenName>Juniper</ns1:GivenName>'
            . '<ns1:Surname>Test</ns1:Surname>'
            . '</ns1:PersonName>'
            . '<ns1:Telephone PhoneNumber="(703)5555555"/>'
            . '<ns1:Document DocID="12345678A" />'
            . '<ns1:Email>user@provider.com</ns1:Email>'
            . '<ns1:Address>'
            . '<ns1:AddressLine>My address</ns1:AddressLine>'
            . '<ns1:CityName>Palma de Mallorca</ns1:CityName>'
            . '<ns1:PostalCode>07000</ns1:PostalCode>'
            . '<ns1:CountryName Code="IR">Iran</ns1:CountryName>'
            . '</ns1:Address>'
            . '</ns1:Customer>'
            . '</ns1:TPA_Extensions>';
             * 
             */
    $TravelInfo = '<ns1:TravelerInfo>'
            . '<ns1:AirTraveler PassengerTypeCode="ADT" BirthDate="1982-04-03T20:00:00">'
            /*
            . '<ns1:ProfileRef>'
            . '<ns1:UniqueID URL="http://www.usda.gov" Type="21" Instance="1999-02- 28T09:13:53" ID="ID12345"/>'
            . '</ns1:ProfileRef>'
            */
            . '<ns1:PersonName>'
            . '<ns1:GivenName>Test</ns1:GivenName>'
            . '<ns1:Surname>Test</ns1:Surname>'
            . '<ns1:NameTitle>MR</ns1:NameTitle>'
            . '</ns1:PersonName>'
            . '<ns1:Telephone AreaCityCode="51"  CountryAccessCode="98" PhoneNumber="37648023"/>'
            . '<ns1:Email>m.mirsamie@gmail.com</ns1:Email>'
            . '<ns1:Address>'
            . '<ns1:CountryName Code="IR" />'
            /*
            . '<ns1:AddressLine>My address</ns1:AddressLine>'
            . '<ns1:CityName>Palma de Mallorca</ns1:CityName>'
            . '<ns1:PostalCode>07000</ns1:PostalCode>'
            . '<ns1:CountryName Code="ES">Spain</ns1:CountryName>'
            */
            . '</ns1:Address>'
            . '<ns1:Document DocHolderNationality="AE" />'
            . '<ns2:TravelerRefNumber RPH="A1" />'
            //. '<ns1:CustLoyalty ProgramID="AA" MembershipID="Q56GTF"/>'
            . '</ns1:AirTraveler>'
            /*
            . '<ns1:SpecialReqDetails>'
            . '<ns1:SeatRequests>'
            . '<ns1:SeatRequest SeatNumber="14C" TravelerRefNumberRPHList="1" FlightRefNumberRPHList="1"></ns1:SeatRequest>'
            . '</ns1:SeatRequests>'
            . '<ns1:SpecialServiceRequests>'
            . '<ns1:SpecialServiceRequest SSRCode="WCHR" TravelerRefNumberRPHList="1" FlightRefNumberRPHList="1">'
            . '<ns1:Airline> CompanyShortName="W5"</ns1:Airline>'
            . '</ns1:SpecialServiceRequest>'
            . '</ns1:SpecialServiceRequests>'
            . '</ns1:SpecialReqDetails>'
            */
            . '</ns1:TravelerInfo>';
    $Fulfill = '<ns1:Fulfillment>'
            . '<ns1:PaymentDetails>'
            . '<ns1:PaymentDetail>'
            . '<ns1:DirectBill>'
            . '<ns1:CompanyName Code="MHD213"/>'
            . '</ns1:DirectBill>'
            . '<ns1:PaymentAmount Amount="180000.00" CurrencyCode="IRR" DecimalPlaces="2"/>'
            . '</ns1:PaymentDetail>'
            . '</ns1:PaymentDetails>'
            . '</ns1:Fulfillment>';
    $bookreq = [
        "EchoToken" => "11868765275150-1300257933",
        "SequenceNmbr" => "1",
        "TimeStamp" => date("Y-m-dTH:i:s"),
        "TransactionIdentifier" => "TID$127106978850125-1713595275", 
        "Version" => "20061.00",
        "POS" => new \SoapVar('<ns1:POS><ns1:Source TerminalID="'.$TerminalID.'" ><ns1:RequestorID ID="'.$AgencyUser.'" Type="4" /><ns1:BookingChannel Type="12" /></ns1:Source></ns1:POS>',\XSD_ANYXML),
        "AirItinerary" => new \SoapVar($AirIt,\XSD_ANYXML),
        //"PriceInfo" => new SoapVar($PriceInfo,XSD_ANYXML),
        "TPA_Extensions" => new \SoapVar($TPA,\XSD_ANYXML),
        "TravelerInfo" => new \SoapVar($TravelInfo,\XSD_ANYXML),
        //"Ticketing" => new SoapVar('<ns1:Ticketing TicketType="eTicket"/>',XSD_ANYXML)
        "Fulfillment" => new \SoapVar($Fulfill,\XSD_ANYXML)
        ];
    $Conact = '<ns2:ContactInfo>'
            . '<ns2:PersonName>'
            . '<ns2:Title>MR</ns2:Title>'
            . '<ns2:FirstName>TEST</ns2:FirstName>'
            . '<ns2:LastName>TEST</ns2:LastName>'
            . '</ns2:PersonName>'
            . '<ns2:Telephone>'
            . '<ns2:PhoneNumber>37648023</ns2:PhoneNumber>'
            . '<ns2:CountryCode>98</ns2:CountryCode>'
            . '<ns2:AreaCode>51</ns2:AreaCode>'
            . '</ns2:Telephone>'
            . '<ns2:Email>m.mirsamie@gmail.com</ns2:Email>'
            . '<ns2:Address>'
            . '<ns2:CountryName>'
            . '<ns2:CountryName>Iran</ns2:CountryName>'
            . '<ns2:CountryCode>IR</ns2:CountryCode>'
            . '</ns2:CountryName>'
            . '<ns2:CityName>Tehran</ns2:CityName>'
            . '</ns2:Address>'
            . '</ns2:ContactInfo>';
    $bookreqex = array(
        "ContactInfo" => new SoapVar($Conact,XSD_ANYXML)
    );
    $out = $client->book($bookreq,$bookreqex);
    /*
    if(is_soap_fault($out))
    {
        var_dump($out);
    }
    else
    {
     * 
     */
    //echo str_ireplace('><', ">\n<",$client->__getLastRequest());
    echo "\nREQUEST : \n".str_ireplace('><', ">\n<",$client->__getLastRequest());
    echo "\nRESPONSE : \n".str_ireplace('><', ">\n<",$client->__getLastResponse())."\n";
    //var_dump($out);
    //}
?>