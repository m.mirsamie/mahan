<?php

class MahanWebService {

    public $client;
    public $wsdl = "http://mahanair.isaaviations.com/webservices/services/AAResWebServices?wsdl";
    public $AgencyUser = 'WSYASTOOSKBL';
    public $AgencyPass = 'p@ss1234';
    public $TerminalID = "MHD213";

    public function __construct($params = array()) {
        if (isset($params['wsdl']) && trim($params['wsdl']) != '') {
            $this->wsdl = $params['wsdl'];
        }
        if (isset($params['AgencyUser']) && trim($params['AgencyUser']) != '') {
            $this->AgencyUser = $params['AgencyUser'];
        }
        if (isset($params['TerminalID']) && trim($params['TerminalID']) != '') {
            $this->TerminalID = $params['TerminalID'];
        }
        if (isset($params['AgencyPass']) && trim($params['AgencyPass']) != '') {
            $this->AgencyPass = $params['AgencyPass'];
        }
        $this->client = new SoapClient($this->wsdl, array("trace" => 1));
        $this->client->__setSoapHeaders($this->soapClientWSSecurityHeader($this->AgencyUser, $this->AgencyPass));
    }

    public function generateWSSecurity($user, $password) {
        $tm_created = gmdate('Y-m-d\TH:i:s\Z');
        $tm_expires = gmdate('Y-m-d\TH:i:s\Z', gmdate('U') + 180);
        $simple_nonce = mt_rand();
        $encoded_nonce = base64_encode(pack('H*', $simple_nonce));
        $passdigest = base64_encode(pack('H*', sha1(pack('H*', $simple_nonce) . pack('a*', $tm_created) . pack('a*', $password))));
        $ns_envelope = 'http://schemas.xmlsoap.org/soap/envelope/';
        $ns_wsse = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
        $ns_wsu = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';
        $password_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText';
        $encoding_type = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary';
        $root = new \SimpleXMLElement('<root/>');
        $envelope = $root->addChild('soap:Envelope', null, $ns_envelope);
        $soapheader = $envelope->addChild('soap:Header');
        $security = $soapheader->addChild('wsse:Security', null, $ns_wsse);
        $security->addAttribute('soap:mustUnderstand', '1');
        $usernameToken = $security->addChild('wsse:UsernameToken', null, $ns_wsse);
        $usernameToken->addChild('wsse:Username', $user, $ns_wsse);
        $password = $usernameToken->addChild('wsse:Password', $password, $ns_wsse);
        $password->addAttribute('Type', $password_type);
        $root->registerXPathNamespace('soap', $ns_envelope);
        $full = $root->xpath('/root/soap:Envelope');
        $auth = $full[0]->asXML();
        return $auth;
    }

    public function soapClientWSSecurityHeader($user, $password) {
        return new \SoapHeader('http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd', 'Security', new \SoapVar($this->generateWSSecurity($user, $password), XSD_ANYXML), true
        );
    }

    public function createBookData($adl, $chd, $inf) {
        $out = '';
        $out .= '<ns1:PassengerTypeQuantity Code="ADT" Quantity="' . $adl . '"/>';
        if ($chd > 0)
            $out .= '<ns1:PassengerTypeQuantity Code="CNN" Quantity="' . $chd . '"/>';
        if ($inf > 0)
            $out .= '<ns1:PassengerTypeQuantity Code="INF" Quantity="' . $inf . '"/>';
        return($out);
    }

    public function getAvailability($from, $to, $DepDate, $adl, $chd, $inf) {
        $client = $this->client;
        $BookData = $this->createBookData($adl, $chd, $inf);
        $param = array(
            "POS" => new SoapVar('<ns1:POS><ns1:Source TerminalID="' . $this->TerminalID . '" ><ns1:RequestorID ID="' . $this->AgencyUser . '" Type="4" /><ns1:BookingChannel Type="12" /></ns1:Source></ns1:POS>', XSD_ANYXML),
            "TravelerInfoSummary" => new SoapVar('<ns1:TravelerInfoSummary><ns1:AirTravelerAvail>' . $BookData . '</ns1:AirTravelerAvail></ns1:TravelerInfoSummary>', XSD_ANYXML),
//            "OriginDestinationInformation" => array(new SoapVar('<ns1:OriginDestinationInformation>
//                        <ns1:DepartureDateTime>' . $DepDate . '</ns1:DepartureDateTime>'
//                    . '<ns1:OriginLocation LocationCode="' . $to . '"/>'
//                    . '<ns1:DestinationLocation LocationCode="' . $from . '"/>'
//                    . '</ns1:OriginDestinationInformation>', XSD_ANYXML), 
//                new SoapVar('<ns1:OriginDestinationInformation>
//                        <ns1:DepartureDateTime>2015-09-19T00:00:00</ns1:DepartureDateTime>'
//                    . '<ns1:OriginLocation LocationCode="' . $to . '"/>'
//                    . '<ns1:DestinationLocation LocationCode="' . $from . '"/>'
//                    . '</ns1:OriginDestinationInformation>', XSD_ANYXML))
            "OriginDestinationInformation" => new SoapVar('<ns1:OriginDestinationInformation>
                        <ns1:DepartureDateTime>' . $DepDate . '</ns1:DepartureDateTime>'
                    . '<ns1:OriginLocation LocationCode="' . $to . '"/>'
                    . '<ns1:DestinationLocation LocationCode="' . $from . '"/>'
                    . '</ns1:OriginDestinationInformation>', XSD_ANYXML)
        );
        $out = $client->getAvailability($param);
        echo "REQUEST : \n".$client->__getLastRequest()."\n";
        $resData = $client->__getLastResponse();
        echo "RESPONSE : \n".$resData;
        $xml = new DOMDocument();
        $xml->loadXML($resData);
        $errors = $xml->getElementsByTagName("Error");
        $err_msg = array();
        $flight = array();
        if ($errors->length > 0) {

            for ($i = 0; $i < $errors->length; $i++) {

                $err_msg[] = array("Code" => $errors->item($i)->attributes->getNamedItem("Code")->value, "Msg" => $errors->item($i)->attributes->getNamedItem("ShortText")->value);
            }
        } else {
            $res = $xml->getElementsByTagName("FlightSegment");
            for ($i = 0; $i < $res->length; $i++) {
                if ($res->item($i)->parentNode->parentNode->parentNode->parentNode->nodeName == "ns1:PricedItinerary") {

                    $fl = array(
                        'FlightNumber' => $res->item($i)->attributes->getNamedItem("FlightNumber")->value,
                        'ArrivalDateTime' => $res->item($i)->attributes->getNamedItem("ArrivalDateTime")->value,
                        'DepartureDateTime' => $res->item($i)->attributes->getNamedItem("DepartureDateTime")->value,
//                        'JourneyDuration' => $res->item($i)->attributes->getNamedItem("JourneyDuration")->value,
                        'RPH' => $res->item($i)->attributes->getNamedItem("RPH")->value,
//                        'ArrivalAirport' => array("text" => $res->item($i)->parentNode->parentNode->previousSibling->nodeValue, "iata" => $res->item($i)->parentNode->parentNode->previousSibling->attributes->getNamedItem('LocationCode')->value),
//                        'OriginLocation' => array("text" => $res->item($i)->parentNode->parentNode->previousSibling->previousSibling->nodeValue, "iata" => $res->item($i)->parentNode->parentNode->previousSibling->previousSibling->attributes->getNamedItem('LocationCode')->value)
                    );
                    $flight[] = $fl;
                }
            }
        }

        return(array(
            "output" => $out,
            "xml" => $resData,
            "flights" => $flight,
            "errors" => $err_msg
        ));
    }

}
